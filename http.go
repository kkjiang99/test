package main

import (
    "fmt"
    "net/http"
    "os"
)

func hello(w http.ResponseWriter, req *http.Request) {
	hName, _ := os.Hostname()
	fmt.Fprintf(w, "hello from go service H:%v\n", hName)
}

func headers(w http.ResponseWriter, req *http.Request) {

    for name, headers := range req.Header {
        for _, h := range headers {
            fmt.Fprintf(w, "%v: %v\n", name, h)
        }
    }
}

func main() {

    http.HandleFunc("/hello", hello)
    http.HandleFunc("/headers", headers)

    http.ListenAndServe(":80", nil)
}
